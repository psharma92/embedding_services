from sentence_transformers import SentenceTransformer
import tensorflow_hub as hub
import numpy as np
import tensorflow_text as text


BERT_MODELS = {
    'en': SentenceTransformer('bert-base-nli-stsb-mean-tokens'),
    'fr': SentenceTransformer('distiluse-base-multilingual-cased')
}

USE_MODELS = {
    'en': hub.load("https://tfhub.dev/google/universal-sentence-encoder/4"),
    'fr': hub.load("https://tfhub.dev/google/universal-sentence-encoder-multilingual/3")
}


def bert_embed(texts: list, lang: str):
    """
    returns USE embeddings
    :param texts: takes list of sentences
    :param lang: language for model
    :return:  matrix of embeddings
    """

    if lang == 'en':
        bert_sent = BERT_MODELS.get('en')
    else:
        bert_sent = BERT_MODELS.get('fr')
    emb = bert_sent.encode(texts)
    emb = [item.tolist() for item in emb]
    return emb




def use_embed(texts: list, lang: str):
    """
    returns USE embeddings
    :param texts: takes list of sentences
    :param lang: language for model
    :return:  matrix of embeddings
    """

    if lang == 'en':
        embed_model = USE_MODELS.get('en')
    else:
        embed_model = USE_MODELS.get('fr')

    embeddings = embed_model(texts)
    embeddings = np.array(embeddings)
    return embeddings.tolist()

