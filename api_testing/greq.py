import json
import grequests
urls = ['http://54.75.39.87/bert'] * 100

d = {
     'sentences': [
                   'I am setence one',
                   'I am sentence two' ],
     'lang': 'pt'
     }

d = json.dumps(d)

rs = (grequests.post(u, data=d) for u in urls)
# call all at same time
x = grequests.map(rs)

print(x)