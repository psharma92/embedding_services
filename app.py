from flask import Flask, request
from embedding_deployment.embedding_service_aws.embedding_funcs import *
from flask_restx import Api, Resource,  reqparse

app = Flask(__name__)
api = Api(app)

parser = reqparse.RequestParser()
parser.add_argument('sentences', type=list, location='json')
parser.add_argument('lang', type=str, location='json')


@api.route('/use')
class USEembeddingService(Resource):
    @api.expect(parser)
    def post(self):
        response = request.get_json(force=True)
        print(response)
        l_sentences = response['sentences']
        print(l_sentences)
        language = response['lang']
        sentences_embeddings = use_embed(l_sentences, language)
        return {'use_embeddings': sentences_embeddings}


@api.route('/bert')
class BERTembeddingService(Resource):
    @api.expect(parser)
    def post(self):
        response = request.get_json(force=True)
        l_sentences = response['sentences']
        language = response['lang']
        sentences_embeddings = bert_embed(l_sentences, language)
        return {'bert_embeddings': sentences_embeddings}


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)


