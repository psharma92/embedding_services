FROM python:3.6
MAINTAINER prafull.sharma@naister.com

RUN apt-get update -y

WORKDIR /opt/

COPY . /opt/

RUN pip3 install -r requirements.txt

EXPOSE 80

CMD [ "python", "app.py" ]